<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\DriverController;
use App\Http\Controllers\Api\VehicleController;

Route::prefix('v1')->group(function(){
	Route::get('/vehicles',[ VehicleController::class,'get' ]);
	Route::post('/vehicle/create',[ VehicleController::class,'store' ]);
	Route::post('/vehicle/update',[ VehicleController::class,'update' ]);
	Route::post('/vehicle/delete',[ VehicleController::class,'destroy' ]);

	Route::get('/drivers',[ DriverController::class,'get' ]);
	Route::post('/driver/create',[ DriverController::class,'store' ]);
	Route::post('/driver/update',[ DriverController::class,'update' ]);
	Route::post('/driver/delete',[ DriverController::class,'destroy' ]);

	Route::post('/auth',[ AuthController::class,'login' ]);
	Route::post('/logout',[ AuthController::class,'logout' ]);
});
