<?php

namespace App\Models;

use App\Models\Driver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Vehicle extends Model
{
    use HasFactory, SoftDeletes;

    //Tabla referencial
    protected $table = "vehicles";

    //Llave primaria
    protected $primaryKey = 'uuid';

    //Tipo de llave primaria
    protected $keyType = 'string';

    //Incrementar la llave primaria
    public $incrementing = false;

    //Campos para agregar masivamente
    protected $fillable = [
        'uuid','fk_driver_id','marca','modelo','placa'
    ];

    //Ocultar elementos en la peticion
    protected $hidden = [
        'fk_driver_id','created_at','updated_at','deleted_at'
    ];

    //Establecer columnas de fechas
    public $timestamps = [
        'created_at','updated_at','deleted_at'
    ];

    //Relacion uno a uno con la tabla driver
    public function drivers()
    {
        return $this->hasOne(Driver::class,'uuid','fk_driver_id');
    }

    //Obtener todos los registros
    public function find()
    {
    	$vehicles = self::whereNull('deleted_at')->with('drivers')->get();
    	return $vehicles;
    }

    //Guardar un nuevo registro
    public function saveOne($vehicle)
    {
        $fkDriverId = empty($vehicle['fk_driver_id']) ? null : $vehicle['fk_driver_id'];

        $isCreate = self::insert([
            'uuid'=> Str::uuid(),
            'fk_driver_id'=> $fkDriverId,
    		'marca'	=>	$vehicle['marca'],
    		'modelo'=>	$vehicle['modelo'],
    		"placa"	=>	$vehicle['placa'],
    		"created_at" => Carbon::now()
    	]);

    	return $isCreate;
    }

    //Actualizar un registro
    public function updateOne($vehicle)
    {
        //Verificar si existe la placa diferente al registro
        $isExistPLaca = DB::table('vehicles')
                        ->where('placa', '=', $vehicle['placa'])
                        ->where('uuid', '<>', $vehicle['uuid'])
                        ->first();

        if($isExistPLaca){
           return false;
        }else{
            $fkDriverId = empty($vehicle['fk_driver_id']) ? null : $vehicle['fk_driver_id'];

            $isUpdate = self::where('uuid','=',$vehicle['uuid'])
        			->update([
                        'fk_driver_id' => $fkDriverId,
        	    		'marca'	=>	$vehicle['marca'],
        	    		'modelo'=>	$vehicle['modelo'],
        	    		"placa"	=>	$vehicle['placa'],
        	    		"updated_at" => Carbon::now()
        	    	]);

        	return $isUpdate;
        }
    }

    //Eliminar un registro
    public function deleteOne($vehicle)
    {
    	$isDelete = self::where('uuid', '=', $vehicle['uuid'])->delete();
    	return $isDelete;
    }
}
