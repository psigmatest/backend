<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    //Nombre de la tabla referencial
    protected $table = 'users';

    //Campos para agregar masivamente
    protected $fillable = [
        'name', 'email', 'password',
    ];

    //Llave primaria
    protected $primaryKey = 'uuid';

    //Tipo de llave primaria
    protected $keyType = 'string';

    //Incrementar la llave primaria
    public $incrementing = false;

    //Ocultar elementos en la peticion
    protected $hidden = [
        'password', 'remember_token',
    ];

    //Configuracion de campos
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
