<?php

namespace App\Models;

use App\Models\Vehicle;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Driver extends Model
{
    use HasFactory, SoftDeletes;

    //Tabla referencial
    protected $table = "drivers";

    //Llave primaria
    protected $primaryKey = 'uuid';

    //Tipo de llave primaria
    protected $keyType = 'string';

    //Incrementar la llave primaria
    public $incrementing = false;

    //Campos para agregar masivamente
    protected $fillable = [
        'uuid',
    	'nombres',
    	'apellidos',
    	'identificacion',
    	'direccion',
    	'telefono',
    	'ciudad_nacimiento',
    ];

    //Ocultar elementos en la peticion
    protected $hidden = [
        'created_at','updated_at','deleted_at'
    ];

    //Establecer columnas de fechas
    public $timestamps = [
        'created_at','updated_at','deleted_at'
    ];


    //Relacion uno a muchos con la tabla vehicles
    public function vehicles()
    {
        return $this->hasMany(Vehicle::class,'fk_driver_id');
    }

    //Obtener todos los registros
    public function find()
    {
    	$driver = self::whereNull('deleted_at')->with('vehicles')->get();
    	return $driver;
    }

    //Guardar un nuevo registro
    public function saveOne($driver)
    {
    	$isCreate = self::insert([
            'uuid'=>Str::uuid(),
    		'nombres'=>	$driver['nombres'],
    		"apellidos"	=>	$driver['apellidos'],
    		"identificacion" =>	$driver['identificacion'],
    		"direccion"	=>	$driver['direccion'],
    		"telefono"	=>	$driver['telefono'],
    		"ciudad_nacimiento"	=>	$driver['ciudad_nacimiento'],
    		"created_at" => Carbon::now()
    	]);

    	return $isCreate;
    }

    //Actualizar un registro
    public function updateOne($driver)
    {
    	$isUpdate = self::where('uuid','=',$driver['uuid'])
			->update([
	    		'nombres'=>	$driver['nombres'],
	    		"apellidos"	=>	$driver['apellidos'],
	    		"identificacion" =>	$driver['identificacion'],
	    		"direccion"	=>	$driver['direccion'],
	    		"telefono"	=>	$driver['telefono'],
	    		"ciudad_nacimiento"	=>	$driver['ciudad_nacimiento'],
	    		"created_at" => Carbon::now()
	    	]);

    	return $isUpdate;
    }

    //Eliminar un registro
    public function deleteOne($driver)
    {
    	$isDelete = self::where('uuid', '=', $driver['uuid'])->delete();
    	return $isDelete;
    }
}
