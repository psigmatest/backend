<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Vehicle;

class VehicleController extends Controller
{

    //Retornar todos los registros
    public function get()
    {
        $instanceVehicle = new Vehicle();

        return response()->json([
            'status'=>200,
            'data'=>$instanceVehicle->find()
        ], 200);
    }

    //Guardar un registro
    public function store(Request $request)
    {
        $instanceVehicle = new Vehicle();

        //Validar los campos obligatorios
        $validator = Validator::make($request->all(), [
                'fk_driver_id' => 'string|nullable',
                'marca' => 'required|string|max:100',
                'modelo' => 'required|string|max:100',
                'placa' => 'required|string|unique:vehicles',
        ]);

        if ($validator->fails()) {
           return response()->json([
            'status'=>404,
            'errors'=>$validator->errors()
           ], 404);
        }else{
            $isCreate = $instanceVehicle->saveOne($request->all());

            if($isCreate){
                return response()->json([
                    'status'=>200,
                    'message'=> 'El vehiculo ah sido agregado !'
                ],200);
            }else{
                return response()->json([
                    'status'=>404,
                    'message'=> 'Se presento un problema en el servidor !'
                ],404);
            }
        }
    }

    //Actualizar un registro
    public function update(Request $request)
    {
        $instanceVehicle = new Vehicle();

        //Validar los campos obligatorios
        $validator = Validator::make($request->all(), [
                'uuid' => 'required|string',
                'fk_driver_id' => 'string|nullable',
                'marca' => 'required|string|max:100',
                'modelo' => 'required|string|max:100',
                'placa' => 'required|string',
        ]);

        if ($validator->fails()) {
           return response()->json([
            'status'=>400,
            'errors'=>$validator->errors()
           ], 404);
        }else{
            $isUpdate = $instanceVehicle->updateOne($request->all());

            if($isUpdate){
                return response()->json([
                    'status'=>200,
                    'message'=> 'El vehiculo ah sido actualizado !'
                ],200);
            }else{
                return response()->json([
                    'status'=>404,
                    'message'=> 'Ya existe esta placa en nuestro registros !'
                ],404);
            }
        }
    }

    //Eliminar un registro
    public function destroy(Request $request)
    {
        $instanceVehicle = new Vehicle();

        //Validar los campos obligatorios
        $validator = Validator::make($request->all(), [
            'uuid' => 'required|string'
        ]);

        if ($validator->fails()) {
           return response()->json([
            'status'=>400,
            'errors'=>$validator->errors()
           ], 404);
        }else{
            $isDelete = $instanceVehicle->deleteOne($request->all());

            if($isDelete){
                return response()->json([
                    'status'=>200,
                    'message'=> 'El vehiculo ah sido eliminado !'
                ],200);
            }else{
                return response()->json([
                    'status'=>404,
                    'message'=> 'Se presento un problema en el servidor !'
                ],404);
            }
        }
    }
}
