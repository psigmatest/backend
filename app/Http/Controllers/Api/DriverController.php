<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Driver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DriverController extends Controller
{
    //Retornar todos los registros
    public function get()
    {
        $instanceDriver = new Driver();

        return response()->json([
            'status'=>200,
            'data'=>$instanceDriver->find()
        ], 200);
    }

    //Guardar un registro
    public function store(Request $request)
    {
        $instanceDriver = new Driver();

        //Validar los campos obligatorios
        $validator = Validator::make($request->all(), [
            'nombres' => 'required|string|max:100',
            'identificacion' => 'required|integer',
            'direccion' => 'required|string|max:100',
            'telefono' => 'required|integer',
            'ciudad_nacimiento' => 'required|string|max:100',
        ]);

        if ($validator->fails()) {
           return response()->json([
            'status'=>404,
            'errors'=>$validator->errors()
           ], 404);
        }else{
            $isCreate = $instanceDriver->saveOne($request->all());

            if($isCreate){
                return response()->json([
                    'status'=>200,
                    'message'=> 'El conductor ah sido agregado !'
                ],200);
            }else{
                return response()->json([
                    'status'=>404,
                    'message'=> 'Se presento un problema en el servidor !'
                ],404);
            }
        }
    }

    //Actualizar un registro
    public function update(Request $request)
    {
        $instanceDriver = new Driver();

        //Validar los campos obligatorios
        $validator = Validator::make($request->all(), [
            "uuid"=> 'required|string',
            'nombres' => 'required|string|max:100',
            'identificacion' => 'required|integer',
            'direccion' => 'required|string|max:100',
            'telefono' => 'required|integer',
            'ciudad_nacimiento' => 'required|string|max:100',
        ]);

        if ($validator->fails()) {
           return response()->json([
            'status'=>400,
            'errors'=>$validator->errors()
           ], 404);
        }else{
            $isUpdate = $instanceDriver->updateOne($request->all());

            if($isUpdate){
                return response()->json([
                    'status'=>200,
                    'message'=> 'El conductor ah sido actualizado !'
                ],200);
            }else{
                return response()->json([
                    'status'=>404,
                    'message'=> 'Se presento un problema en el servidor !'
                ],404);
            }
        }
    }

    //Eliminar un registro
    public function destroy(Request $request)
    {
        $instanceDriver = new Driver();

        //Validar los campos obligatorios
        $validator = Validator::make($request->all(), [
            'uuid' => 'required|string'
        ]);

        if ($validator->fails()) {
           return response()->json([
            'status'=>400,
            'errors'=>$validator->errors()
           ], 404);
        }else{
            $isDelete = $instanceDriver->deleteOne($request->all());

            if($isDelete){
                return response()->json([
                    'status'=>200,
                    'message'=> 'El conductor ah sido eliminado !'
                ],200);
            }else{
                return response()->json([
                    'status'=>404,
                    'message'=> 'Se presento un problema en el servidor !'
                ],404);
            }
        }
    }
}
