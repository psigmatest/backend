<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\Token;

class AuthController extends Controller
{
    protected $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            $this->user = Auth::user();

            return $next($request);
        });
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email','password');

        // Verificando las credenciales del usuario si fallan
        if(!Auth::attempt($credentials)){
            return response()->json([
                'status'=>400,
                'data'=>[
                    'message'=>'Usuario y/o contraseña incorrecta'
                ],
            ],400);
        }

        //Creando token de usuario
        $access_token = Auth::user()->createToken('OATH_API')->accessToken;

        return response()->json([
                'status'=>201,
                'data'=>[
                    'access_token'=>$access_token
                ],
        ],201);
    }

    public function logout(Request $request)
    {
        $access_token = $request->header('Authorization');

        if($access_token){
            $idUser = $this->getTokenId($access_token);
    
            $isDelete = DB::table('oauth_access_tokens')->where('id', '=', $idUser)->delete();
            
            if($isDelete){
                return response()->json([
                    'status'=>201,
                    'message'=>'Token eliminado',
                ],201); 
            }
        }
    }

    public function getTokenId($token)
    {
        $auth_header = explode(' ', $token);
        $token = $auth_header[1];
        $token_parts = explode('.', $token);
        $token_header = $token_parts[1];
        $token_header_json = base64_decode($token_header);
        $token_header_array = json_decode($token_header_json, true);
        $token_id = $token_header_array['jti'];
        return $token_id;
    }
}
