<?php

namespace Database\Factories;

use App\Models\Driver;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DriverFactory extends Factory
{

    protected $model = Driver::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'uuid'=>Str::uuid(),
            'nombres' =>$this->faker->name,
            'apellidos' =>$this->faker->name,
            'identificacion' =>$this->faker->ean13,
            'direccion' =>$this->faker->address,
            'telefono' =>$this->faker->ean13,
            'ciudad_nacimiento' =>$this->faker->city,
            'created_at' =>$this->faker->date
        ];
    }
}
