<?php

namespace Database\Factories;

use App\Models\Driver;
use App\Models\Vehicle;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class VehicleFactory extends Factory
{

    protected $model = Vehicle::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'uuid'=>Str::uuid(),
            'fk_driver_id'=> Driver::all()->random()->uuid,
            'marca'=> $this->faker->randomElement(['Bmw','Audi','Lamborguini','Mercedes','Chevrolet','Toyota','Suzuki']),
            'modelo'=>$this->faker->word . $this->faker->randomNumber,
            'placa'=>$this->faker->word . $this->faker->randomNumber,
            'created_at' =>$this->faker->date
        ];
    }
}
