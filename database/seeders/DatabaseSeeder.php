<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\DriverSeeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\VehicleSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
       $this->call(DriverSeeder::class);
       $this->call(VehicleSeeder::class);
    }
}
