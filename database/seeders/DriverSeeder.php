<?php

namespace Database\Seeders;

use App\Models\Driver;
use Illuminate\Database\Seeder;

class DriverSeeder extends Seeder
{

	protected $model = 'drivers';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Driver::factory(7)->create();
    }
}
