<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->insert([
    		'uuid'=>Str::uuid(),
    		'name'=> 'Carlos Perez Fonzi',
    		'email' => 'psigmatest@corporation.com',
    		'password'=>Hash::make('test1234'),
    		'created_at'=>Carbon::now()
    	]);
    }
}
