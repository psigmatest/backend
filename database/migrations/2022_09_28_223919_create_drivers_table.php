<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            //$table->id();
            $table->uuid('uuid')->primary();
            $table->string('nombres');
            $table->string('apellidos');
            $table->integer('identificacion');
            $table->string('direccion');
            $table->integer('telefono');
            $table->string('ciudad_nacimiento');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
