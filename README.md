# Backend dashboard

# instalacion y ejecución
 
    1) Clonar repositio

    2) Crear el archivo .env a raiz del proyecto
       - Copear el contenido de .env.example y pegarlo en .env
       - Configurar llaves
          DB_CONNECTION=sqlite
          DB_DATABASE = C:/path/folderproject/database/psigmadb.sqlite

    3)Ejecutar los siguientes comandos
      composer install
      php artisan key:generate
      php artisan migrate --seed
      php artisan passport:install
      php artisan serve

    4) Servidor corriendo por el puerto `http://127.0.0.1:8000`
    
